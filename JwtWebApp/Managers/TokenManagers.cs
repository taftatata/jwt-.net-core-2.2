﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace JwtWebApp.Managers
{
    public class TokenManagers
    {
        public string _UserName { get; set; }
        public string _Password { get; set; }
        private readonly IConfiguration _configuration;

        public TokenManagers(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GenerateToken()
        {
            var claims = new List<Claim>();
            claims.Add(new Claim("role", "User"));
            claims.Add(new Claim("role", "Admin"));
            claims.Add(new Claim("role", "SuperAdmin"));
            claims.Add(new Claim("email", "tafta@dealpos.com"));


            int expiryInMinutes = Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]);
            var jwtToken = new JwtSecurityToken(
                issuer: _configuration["Jwt:Site"],
                audience: _configuration["Jwt:Site"],
                expires: DateTime.UtcNow.AddMinutes(expiryInMinutes),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256),
                claims: claims
            );

            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            string token = jwtSecurityTokenHandler.WriteToken(jwtToken);
            return token;
        }

        #region Private Methods
        private SecurityKey GetSymmetricSecurityKey()
        {
            var signinKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));
            return signinKey;
        }

        private TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidAudience = _configuration["Jwt:Site"],
                ValidIssuer = _configuration["Jwt:Site"],
                IssuerSigningKey = GetSymmetricSecurityKey()
            };
        }
        #endregion

        #region Token Validation
        /// <summary>
        /// Validates whether a given token is valid or not, and returns true in case the token is valid otherwise it will return false;
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>

        public ClaimsPrincipal TokenValidation(string jwtToken)
        {
            SecurityToken validatedToken;
            var handler = new JwtSecurityTokenHandler();
            handler.InboundClaimTypeMap.Clear();
            var parameters = GetTokenValidationParameters();

            ClaimsPrincipal user = handler.ValidateToken(jwtToken, parameters, out validatedToken);
            return user;
        }
        #endregion

        #region Decode Payload
        public static byte[] Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+'); // 62nd char of encoding
            output = output.Replace('_', '/'); // 63rd char of encoding
            switch (output.Length % 4) // Pad with trailing '='s
            {
                case 0: break; // No pad chars in this case
                case 2: output += "=="; break; // Two pad chars
                case 3: output += "="; break; // One pad char
                default: throw new System.Exception("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output); // Standard base64 decoder
            return converted;
        }
        #endregion

    }
}
