﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JwtWebApp.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace JwtWebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IConfiguration _configuration;
        public AuthController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost]
        public ActionResult GenerateToken([FromBody] JObject req)
        {
            dynamic obj = req;
            string token = string.Empty;
            if (string.IsNullOrEmpty(obj.UserName.ToString()) || string.IsNullOrEmpty(obj.Password.ToString())) return BadRequest("The username and/or password is invalid!");

            if (obj.UserName == "tafta" && obj.Password == "1234") {
                TokenManagers mgr = new TokenManagers(_configuration);
                token = mgr.GenerateToken();
            }
            return Ok(token);
        }
    }
}