﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JwtWebApp.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JwtWebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class DemoController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public DemoController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost]
        public ActionResult GetAuth()
        {
            string authHeader = Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", string.Empty);

            TokenManagers mgr = new TokenManagers(_configuration); 
            ClaimsPrincipal token = mgr.TokenValidation(authHeader);
            if (token != null) 
            {
                var parts = authHeader.Split('.');
                var payload = parts[1];
                var payloadJson = Encoding.UTF8.GetString(TokenManagers.Base64UrlDecode(payload));
                var payloadData = JObject.Parse(payloadJson);
                return Ok(payloadData.ToString());
            }
            else
            {
                throw new ArgumentException("Invalid JWT");
            }
        }

        [HttpPost]
        public ActionResult Post()
        {
            string tokenString = Request.Headers["Authorization"];
            tokenString = tokenString.Replace("Bearer ", string.Empty);

            TokenManagers mgr = new TokenManagers(_configuration);
            ClaimsPrincipal token = mgr.TokenValidation(tokenString);
            if(token != null)
            {
                //JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
                //{
                //    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //};
                //var jsonObject = JsonConvert.SerializeObject(token.Identities, jsonSerializerSettings);
                //return Ok(jsonObject);
                var response = User.Claims.Select(c => new { Type = c.Type, Value = c.Value });
                return Ok(response);
            }
            else
            {
                throw new ArgumentException("Invalid JWT");
            }

        }
    }
}